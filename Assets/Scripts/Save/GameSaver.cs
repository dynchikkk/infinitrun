using Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSaver : Singleton<GameSaver>
{
    private static string HAS_SAVE = "HAS_SAVE";

    public RecordData RecordData { get; private set; } = new();

    public void SaveData()
    {
        PlayerPrefs.SetString(HAS_SAVE, HAS_SAVE);

        PlayerPrefs.SetInt(RecordData.recordString, RecordData.record);
        PlayerPrefs.Save();
        Debug.Log("Data saved");
    }

    public void LoadData()
    {
        if (!PlayerPrefs.HasKey(HAS_SAVE))
        {
            Debug.Log("Data loaded");
            return;
        }

        RecordData.record = PlayerPrefs.GetInt(RecordData.recordString);
        Debug.Log("Data loaded");
    }
}
