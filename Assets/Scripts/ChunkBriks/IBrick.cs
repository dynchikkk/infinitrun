public interface IBrick
{
    bool IsInitialized { get; }
    void RefreshBrick();

    void MakeAction();
}
