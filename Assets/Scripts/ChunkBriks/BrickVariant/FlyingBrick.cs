using DG.Tweening;
using UnityEngine;

public class FlyingBrick : MonoBehaviour, IBrick
{
    public bool IsInitialized { get; private set; }

    private bool _isBeenFly = false;
    private float move = 0;

    public void RefreshBrick()
    {
        if (move != 0)
            transform.position -= new Vector3(0, move, 0);
    }

    public void MakeAction()
    {
        _isBeenFly = true;
        move = Random.Range(1f, 2f);
        transform.DOMoveY(transform.position.y + move, 1);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player") || _isBeenFly)
            return;

        MakeAction();
    }
}
