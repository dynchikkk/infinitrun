using SimpleUI;
using UnityEngine;
using Base;

public class GameLoader : Singleton<GameLoader>
{
    [SerializeField] private int _fps = 120;
    [SerializeField] private Vector3 _characterSpawnPosition = Vector3.zero;

    [Header("Class Init")]
    [SerializeField] private UIManager _uiManager;
    [SerializeField] private FinishController _finishController = new();

    [SerializeField] private Character _character;
    [SerializeField] private ChuncFactory _chuncFactory;

    protected override void Awake()
    {
        base.Awake();

        Application.targetFrameRate = _fps;
        GameSaver.Instance.LoadData();
        Init();
    }

    private void Init()
    { 
        _character.Init();
        _character.transform.position = _characterSpawnPosition;

        _chuncFactory.Init(_character);
        _finishController.Init(_character);

        _uiManager.Init(_character, _finishController);
    }

    public void StartGame()
    {
        _character.transform.position = _characterSpawnPosition;
        _character.StartCharacter();
        
        _chuncFactory.RestartFactory();
    }

    public void RestartGame()
    {
        StartGame();
    }
}
