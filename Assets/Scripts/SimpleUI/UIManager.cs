using Base;
using UnityEngine;
using UnityEngine.TextCore.Text;

namespace SimpleUI
{
    public class UIManager : Singleton<UIManager> // Replace by SingletoneInit
    {
        [SerializeField] private MainMenuView _mainMenuView;
        [SerializeField] private GameMenuView _gameMenuView;
        [SerializeField] private SettingsView _settingsView;
        [SerializeField] private PauseMenuView _pauseMenuView;
        [SerializeField] private EndMenuView _endMenuView;

        public void Init(Character character, FinishController finishController)
        {
            _mainMenuView.Init(_settingsView, _gameMenuView);
            _mainMenuView.Show();

            _gameMenuView.Init(_pauseMenuView, character.RoadCounter);
            _gameMenuView.Hide();

            _settingsView.Init();
            _settingsView.Hide();

            _pauseMenuView.Init(_mainMenuView, _settingsView);
            _pauseMenuView.Hide();

            _endMenuView.Init(_mainMenuView, finishController);
            _endMenuView.Hide();

            character.OnCharacterDead += WhenLoose;
        }

        private void WhenLoose(bool isNewRecord)
        {
            _pauseMenuView.Hide();
            _gameMenuView.Hide();
            _endMenuView.Show();
        }
    }
}
