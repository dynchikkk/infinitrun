using SimpleUI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static UnityEditor.Experimental.AssetDatabaseExperimental.AssetDatabaseCounters;

public class GameMenuView : UIView<GameMenuLogic>
{
    [Header("UI")]
    [SerializeField] private Button _pauseButton;
    [SerializeField] private TMP_Text _passedCount;

    // Views
    private PauseMenuView _pauseMenuView;
    private RoadCounter _counter;

    public void Init(PauseMenuView pauseMenuView, RoadCounter counter)
    {
        base.Init();
        _pauseMenuView = pauseMenuView;

        _counter = counter;
        _logic.SetPassedCountText(counter.CurrentCount);
    }

    protected override GameMenuLogic CreateLogic()
    {
        GameMenuLogic temp = new(_passedCount);
        return temp;
    }

    protected override void SubUI()
    {
        _logic.SetPassedCountText(_counter.CurrentCount);

        _pauseButton.onClick.AddListener(delegate
        {
            _pauseMenuView.Show();
        });

        _counter.OnCurrentCountChange += _logic.SetPassedCountText;
    }

    protected override void UnSubUI()
    {
        _pauseButton.onClick.RemoveAllListeners();

        _counter.OnCurrentCountChange -= _logic.SetPassedCountText;
    }
}
