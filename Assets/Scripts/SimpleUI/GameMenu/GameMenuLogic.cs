using SimpleUI;
using TMPro;

public class GameMenuLogic : UILogic
{
    private TMP_Text _counterText;

    public GameMenuLogic(TMP_Text counterText)
    {
        _counterText = counterText;
    }

    public override void WhenHide()
    {
        //
    }

    public override void WhenShow()
    {
        //
    }

    public void SetPassedCountText(int count)
    {
        _counterText.text = $"Score: {count}";
    }
}
