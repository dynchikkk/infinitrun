using SimpleUI;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsLogic : UILogic
{
    private List<SliderMixerPair> _sliders;
    private AudioMixer _mixer;

    public SettingsLogic(List<SliderMixerPair> sliders, AudioMixer mixer)
    {
        _sliders = sliders;
        _mixer = mixer;
        LoadVolumeValue();
        SetSlidersValue();
    }

    public override void WhenHide()
    {
        SaveVolumeValue();
    }

    public override void WhenShow()
    {
        //LoadVolumeValue();
        //SetSlidersValue();
    }

    public void SetSlidersValue()
    {
        // slider volume set
        foreach (SliderMixerPair pair in _sliders)
        {
            Slider temp = pair.Slider;
            temp.maxValue = 0;
            temp.minValue = -80;
            temp.value = temp.maxValue;
        }
    }

    public void SetVolume(SliderMixerPair pair) =>
        _mixer.SetFloat(pair.ExpParam, pair.Slider.value);

    public SliderMixerPair GetSliderByType(SliderVolume slider)
    {
        return _sliders.Find(x => x.SliderType == slider);
    }

    public void LoadVolumeValue()
    {
        // Take volume value from saves
    }

    public void SaveVolumeValue()
    {
        // Save volume value
    }
}
