using SimpleUI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsView : UIView<SettingsLogic>
{
    [SerializeField] private AudioMixer _mixer;

    [Header("UI")]
    [SerializeField] private List<SliderMixerPair> _sliders = new();
    [SerializeField] private Button _backButton;

    public SettingsLogic Settings => _logic;

    protected override SettingsLogic CreateLogic()
    {
        SettingsLogic temp = new(_sliders, _mixer);
        return temp;
    }

    protected override void SubUI()
    {
        _backButton.onClick.AddListener(delegate
        {
            Hide();
        });

        SliderMixerPair master = Settings.GetSliderByType(SliderVolume.Master);
        master?.Slider.onValueChanged.AddListener(delegate (float n) { Settings.SetVolume(master); });

        SliderMixerPair effects = Settings.GetSliderByType(SliderVolume.Effects);
        effects?.Slider.onValueChanged.AddListener(delegate (float n) { Settings.SetVolume(effects); });

        SliderMixerPair music = Settings.GetSliderByType(SliderVolume.Music);
        music?.Slider.onValueChanged.AddListener(delegate (float n) { Settings.SetVolume(music); });

        SliderMixerPair ui = Settings.GetSliderByType(SliderVolume.UI);
        ui?.Slider.onValueChanged.AddListener(delegate (float n) { Settings.SetVolume(ui); });
    }

    protected override void UnSubUI()
    {
        _backButton.onClick.RemoveAllListeners();

        foreach (SliderMixerPair pair in _sliders)
            pair.Slider.onValueChanged.RemoveAllListeners();
    }
}
