using Base;
using SimpleUI;
using UnityEngine;

public class PauseMenuLogic : UILogic
{
    public override void WhenHide()
    {
       // 
    }

    public override void WhenShow()
    {
        Pause();
    }

    public void Pause() =>
        TimeFreezer.FreezeTime(0);

    public void Resume() =>
        TimeFreezer.FreezeTime(1);

    public void Restart()
    {
        GameLoader.Instance.RestartGame();
    }
}
