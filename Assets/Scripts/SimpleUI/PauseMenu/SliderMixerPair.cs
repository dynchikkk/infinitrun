using System;
using UnityEngine.UI;

public enum SliderVolume
{
    Master = 0,
    Music = 1,
    Effects = 2,
    UI = 3
}

[Serializable]
public class SliderMixerPair
{
    public Slider Slider;
    public SliderVolume SliderType;
    public string ExpParam;
}
