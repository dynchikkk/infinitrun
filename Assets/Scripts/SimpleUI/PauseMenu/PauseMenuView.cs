using SimpleUI;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenuView : UIView<PauseMenuLogic>
{
    //[SerializeField] private AudioMixer _mixer;

    [Header("UI")]
    [SerializeField] private Button _settingsButton;
    [SerializeField] private Button _restartButton;
    [SerializeField] private Button _mainMenuButton;
    [SerializeField] private Button _resumeButton;

    // Views
    private MainMenuView _mainMenuView;
    private SettingsView _settingsView;

    public void Init(MainMenuView mainMenuView, SettingsView settingsView)
    {
        base.Init();
        _mainMenuView = mainMenuView;
        _settingsView = settingsView;
    }

    protected override PauseMenuLogic CreateLogic()
    {
        PauseMenuLogic temp = new PauseMenuLogic();
        return temp;
    }

    protected override void SubUI()
    {
        _restartButton.onClick.AddListener(delegate ()
        {
            _logic.Resume();
            _logic.Restart();
            Hide();
        });
        _mainMenuButton.onClick.AddListener(delegate ()
        {
            _mainMenuView.Show();
            Hide();
        });
        _resumeButton.onClick.AddListener(delegate ()
        {
            _logic.Resume();
            Hide();
        });
        _settingsButton.onClick.AddListener(delegate ()
        {
            _settingsView.Show();
        });
    }

    protected override void UnSubUI()
    {
        _restartButton.onClick.RemoveAllListeners();
        _resumeButton.onClick.RemoveAllListeners();
        _mainMenuButton.onClick.RemoveAllListeners();
    }
}
