namespace SimpleUI
{
    public abstract class UILogic
    {
        //public void Init() { }

        public abstract void WhenShow();

        public abstract void WhenHide();
    }
}
