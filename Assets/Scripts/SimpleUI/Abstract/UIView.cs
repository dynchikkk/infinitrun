using System;
using UnityEngine;

namespace SimpleUI
{
    public abstract class UIView<T> : MonoBehaviour where T : UILogic
    {
        public event Action OnShow;
        public event Action OnHide;

        protected T _logic;

        public virtual void Init() =>
            _logic = CreateLogic();

        protected abstract T CreateLogic();

        public virtual void Show()
        {
            SubUI();
            gameObject.SetActive(true);
            OnShow?.Invoke();
            _logic.WhenShow();
        }

        public virtual void Hide()
        {
            gameObject.SetActive(false);
            OnHide?.Invoke();
            _logic.WhenHide();
            UnSubUI();
        }

        protected abstract void SubUI();
        protected abstract void UnSubUI();

    }
}
