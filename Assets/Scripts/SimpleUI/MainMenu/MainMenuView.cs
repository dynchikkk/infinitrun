using Base;
using SimpleUI;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuView : UIView<MainMenuLogic>
{
    [Header("UI")]
    [SerializeField] private Button _startButton;
    [SerializeField] private Button _settingsButton;

    // Views 
    private SettingsView _settingsView;
    private GameMenuView _gameMenuView;

    public void Init(SettingsView settingsView, GameMenuView gameMenuView)
    {
        base.Init();
        _gameMenuView = gameMenuView;
        _settingsView = settingsView;
    }

    protected override MainMenuLogic CreateLogic()
    {
        MainMenuLogic temp = new();
        return temp;
    }

    protected override void SubUI()
    {
        _startButton.onClick.AddListener(delegate
        {
            _logic.StartGame();
            TimeFreezer.FreezeTime(1);
            Hide();
            _gameMenuView.Show();
        });

        _settingsButton.onClick.AddListener(delegate
        {
            _settingsView.Show();
        });
    }

    protected override void UnSubUI()
    {
        _startButton.onClick.RemoveAllListeners();
        _settingsButton.onClick.RemoveAllListeners();
    }
}
