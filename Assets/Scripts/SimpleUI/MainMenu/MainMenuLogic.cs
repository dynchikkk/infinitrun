using Base;
using SimpleUI;

public class MainMenuLogic : UILogic
{
    public override void WhenHide()
    {
        //
    }

    public override void WhenShow()
    {
        //
    }

    public void StartGame()
    {
        // Game start logic
        TimeFreezer.FreezeTime(1);
        GameLoader.Instance.StartGame();
    }
}
