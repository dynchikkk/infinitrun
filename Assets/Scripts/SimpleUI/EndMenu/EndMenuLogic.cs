using DG.Tweening;
using SimpleUI;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EndMenuLogic : UILogic
{
    private TMP_Text _counterText;
    private TMP_Text _recordText;

    public EndMenuLogic (TMP_Text counterText, TMP_Text recordText)
    {
        _counterText = counterText;
        _recordText = recordText;
    }

    public override void WhenHide()
    {
        //
    }

    public override void WhenShow()
    {
        //
    }

    public void SetAllCountText(bool isNewRecord, int currentVal, int record)
    {
        _counterText.text = $"Your Score: {currentVal}";

        if (isNewRecord)
        {
            _recordText.text = $"NEW RECORD: {record}";
            _recordText.color = Color.red;
        }
        else
        {
            _recordText.text = $"Yoir record: {record}";
            _recordText.color = Color.black;
        }
    }

}
