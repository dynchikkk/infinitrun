using SimpleUI;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EndMenuView : UIView<EndMenuLogic>
{
    [Header("UI")]
    [SerializeField] private Button _returnButton;
    [SerializeField] private TMP_Text _counterText;
    [SerializeField] private TMP_Text _recordText;

    // Views
    private MainMenuView _mainMenuView;
    private FinishController _finishController;

    public void Init(MainMenuView mainMenuView, FinishController finishController)
    {
        base.Init();
        _finishController = finishController;
        _mainMenuView = mainMenuView;
        _finishController.OnFinish += _logic.SetAllCountText;
    }

    protected override EndMenuLogic CreateLogic()
    {
        EndMenuLogic temp = new EndMenuLogic(_counterText, _recordText);
        return temp;
    }

    protected override void SubUI()
    {
        _returnButton.onClick.AddListener(delegate
        {
            _mainMenuView.Show();
            Hide();
        });

        
    }

    protected override void UnSubUI()
    {
        _returnButton.onClick.RemoveAllListeners();

        //_finishController.OnFinish -= _logic.SetAllCountText;
    }
}
