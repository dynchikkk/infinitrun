using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Character : MonoBehaviour, IMovable
{
    public event Action<bool> OnCharacterDead;

    public RoadCounter RoadCounter { get; private set;/*init;*/ }/* = new RoadCounter();*/
    public RecordHolder RecordHolder { get; private set; }

    [SerializeField] private float _speed = 2;

    private Rigidbody _rb = null;
    private Vector3 _moveDirection = Vector3.zero;
    private bool _isInitialized = false;
    private Movement _movement;

    public void Init()
    {
        if (!_isInitialized)
        {
            _rb = GetComponent<Rigidbody>();
            RoadCounter = new RoadCounter();
            RecordHolder = new RecordHolder();
            _movement = GetComponent<Movement>();
            if (_movement == null)
                throw new System.Exception("No movementModule");
            _movement.Init(this);
        }

        RoadCounter.CurrentCount = 0;
        _isInitialized = true;
    }

    public void StartCharacter()
    {
        _rb.velocity = Vector3.zero;
        CameraController.Instance.FocusCameraOnObject(gameObject);
        RoadCounter.CurrentCount = 0;
        _movement.SetMoveCondition(true);
    }

    public void KillPlayer()
    {
        ResetPlayer();
        CameraController.Instance.UnFocusCamera();
        bool isNewRecord = RecordHolder.CheckRecord(RoadCounter.CurrentCount);
        OnCharacterDead?.Invoke(isNewRecord);
        Debug.Log("Dead");

    }

    public void ResetPlayer()
    {
        _rb.velocity = Vector3.zero;
        _movement.SetMoveCondition(false);
    }

    private void FixedUpdate()
    {
        MoveInternal();
    }

    public void Move(Vector3 direction) =>
        _moveDirection = direction;

    private void MoveInternal() =>
        _rb.AddForce(_moveDirection * _speed);
}
