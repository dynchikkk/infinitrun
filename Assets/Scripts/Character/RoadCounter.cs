using System;

public class RoadCounter
{
    public event Action<int> OnCurrentCountChange;

    private int _currentCount = 0;
    public int CurrentCount
    {
        get => _currentCount;
        set
        {
            _currentCount = value;
            OnCurrentCountChange?.Invoke(_currentCount);
        }
    }

}
