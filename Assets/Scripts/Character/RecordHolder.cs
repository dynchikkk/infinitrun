using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecordHolder
{
    public int Record { get; private set; }

    public RecordHolder()
    {
        Record = GameSaver.Instance.RecordData.record;
    }

    public bool CheckRecord(int newVal)
    {
        if (newVal > Record)
        {
            Record = newVal;
            GameSaver.Instance.RecordData.record = Record;

            return true;
        }
        return false;
    }
}
