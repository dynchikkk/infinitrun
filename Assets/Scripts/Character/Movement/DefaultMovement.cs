using UnityEngine;

public class DefaultMovement : Movement
{
    private const string HORIZONTAL = "Horizontal";
    private const string VERTICAL = "Vertical";

    protected override void ReadMove()
    {
        var horizontal = Input.GetAxis(HORIZONTAL);
        var vertical = Input.GetAxis(VERTICAL);
        Vector3 direction = new Vector3(horizontal, 0, vertical);

        _movable.Move(direction);
    }
}
