using UnityEngine;

public abstract class Movement : MonoBehaviour
{
    public bool CanMove { get; private set; }
    protected IMovable _movable;

    public void Init(IMovable movable)
    {
        _movable = movable;
        SetMoveCondition(false);
    }

    private void Update()
    {
        if(CanMove)
            ReadMove();
    }

    public void SetMoveCondition(bool cond) =>
        CanMove = cond;

    protected abstract void ReadMove();
}
