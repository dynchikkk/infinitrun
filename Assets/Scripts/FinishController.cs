using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishController
{
    public event Action<bool, int, int> OnFinish;

    private Character _character;

    public void Init(Character character)
    {
        _character = character;
        _character.OnCharacterDead += Finish;
    }

    public void Finish(bool isNewRecord)
    {
        OnFinish?.Invoke(
            isNewRecord, 
            _character.RoadCounter.CurrentCount, 
            _character.RecordHolder.Record
            );

        if (isNewRecord)
            GameSaver.Instance.SaveData();
    }
}
