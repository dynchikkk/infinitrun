using System;
using UnityEngine;

public class ChunkBoundsContoller : MonoBehaviour
{
    public event Action<Collider> OnBeginEnter;
    public event Action<Collider> OnEndEnter;

    private BoundType _boundType = BoundType.Begin;

    public void ResetBound() =>
        gameObject.SetActive(true);

    public void SetBoundType(BoundType boundType) =>
        _boundType = boundType;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player"))
            return;

        switch (_boundType)
        {
            case BoundType.Begin:
                OnBeginEnter?.Invoke(other);
                break;

            case BoundType.End:
                OnEndEnter?.Invoke(other);
                break;
        }

        gameObject.SetActive(false);
    }
}

public enum BoundType
{
    Begin = 0,
    End = 1,
};