using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class FallController : MonoBehaviour
{
    public event Action OnPlayerFall;

    private static string _playerTag = "Player";

    private void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag(_playerTag))
            return;

        OnPlayerFall?.Invoke();
    }

    public void SetEnableState(bool state)
        => gameObject.SetActive(state);
}
