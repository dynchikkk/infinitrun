using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;


public abstract class Chunk : MonoBehaviour, IPoolObject
{
    public event Action<Chunk> OnChunkEnd;
    public event Action<Chunk> OnChunkBegin;
    public event Action<IPoolObject> OnObjectNeededToDeactivate;

    [field: SerializeField] public Transform BeginTransform { get; protected set; }
    [field: SerializeField] public Transform EndTransform { get; protected set; }
    [field: SerializeField] public AnimationCurve SpawnChansePerTime { get; protected set; }
    public FallController FallController { get; protected set; }

    [SerializeField] private float _destroyDelay = 3;

    protected List<IBrick> _bricks = new();
    protected ChunkBoundsContoller _chunkBeginContoller;
    protected ChunkBoundsContoller _chunkEndContoller;

    private bool _isInitialized = false;

    public virtual void Init()
    {
        if (!_isInitialized)
        {
            FindBricks();
            FallController = GetComponentInChildren<FallController>();
        }

        FallController.SetEnableState(false);
        SetBounds();
        _isInitialized = true;
    }

    public virtual void BeginChunk(Collider col)
    {
        OnChunkBegin?.Invoke(this);
        FallController.SetEnableState(true);
    }

    public virtual void EndChunk(Collider col)
    {
        OnChunkEnd?.Invoke(this);
        StartCoroutine(DestroyChunk(_destroyDelay));
    }

    public IEnumerator DestroyChunk(float time)
    {
        yield return new WaitForSeconds(time);
        OnObjectNeededToDeactivate?.Invoke(this);
    }

    protected void SetBounds()
    {
        if (_isInitialized == false)
        {
            _chunkBeginContoller = BeginTransform.AddComponent<ChunkBoundsContoller>();
            _chunkBeginContoller.SetBoundType(BoundType.Begin);
            _chunkEndContoller = EndTransform.AddComponent<ChunkBoundsContoller>();
            _chunkEndContoller.SetBoundType(BoundType.End);
        }

        _chunkEndContoller.ResetBound();
        _chunkBeginContoller.ResetBound();
        _chunkBeginContoller.OnBeginEnter += BeginChunk;
        _chunkEndContoller.OnEndEnter += EndChunk;
    }

    public virtual void RefreshBlocks()
    {
        foreach (IBrick brick in _bricks)
            brick.RefreshBrick();
    }

    public virtual void UnSubChunk()
    {
        _chunkEndContoller.OnEndEnter -= EndChunk;
        _chunkBeginContoller.OnBeginEnter -= BeginChunk;
    }

    [ContextMenu("Add Brics")]
    private void FindBricks()
    {
        var brics = GetComponentsInChildren<IBrick>();
        _bricks.Clear();
        _bricks.AddRange(brics);
    }

    public void ResetBeforeBackToPool()
    {
        UnSubChunk();
        FallController.SetEnableState(false);
        gameObject.SetActive(false);
        RefreshBlocks();
    }
}
