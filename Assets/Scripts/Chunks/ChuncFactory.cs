using Base.Pool;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ChuncFactory : MonoBehaviour
{
    public event Action<Chunk> OnChunkRiched;

    [SerializeField] private List<Chunk> _chunkPrefabs = new();
    [SerializeField] private Chunk _startChunk;
    [SerializeField] private int _startChunkCount;

    private List<Chunk> _spawnedChunks = new();
    private Character _character;
    private Dictionary<Chunk, PoolMono<Chunk>> _chunkPools = new();
    private RoadCounter _roadCounter => _character.RoadCounter;

    private bool _isInitialized = false;

    public void Init(Character character)
    {
        _character = character;

        if (!_isInitialized)
        {
            foreach (Chunk chunk in _chunkPrefabs)
            {
                var temp = new PoolMono<Chunk>(chunk, 5);
                _chunkPools.Add(chunk, temp);
            }
        }

        RestartFactory();

        _isInitialized = true;
    }

    private void InstantiateChunk(Chunk prev = null)
    {
        if (prev?.gameObject.name ==
            _spawnedChunks[Mathf.Clamp(_spawnedChunks.Count - 2, 0, _spawnedChunks.Count)].gameObject.name)
            prev = GetRadomChunk(true);
        else
            prev = GetRadomChunk();

        Chunk spawned = GetPoolByChank(prev).GetObject();// Instantiate(prev, transform);
        spawned.transform.position = _spawnedChunks.Last().EndTransform.position;
        spawned.Init();
        SubChunk(spawned);
        _spawnedChunks.Add(spawned);
    }

    private PoolMono<Chunk> GetPoolByChank(Chunk chunk)
    {
        return _chunkPools[chunk];
    }

    private Chunk GetRadomChunk(bool random = false)
    {
        if (random)
            return _chunkPrefabs[UnityEngine.Random.Range(0, _chunkPrefabs.Count)];

        List<float> chances = new List<float>();
        for (int i = 0; i < _chunkPrefabs.Count; i++)
        {
            chances.Add(_chunkPrefabs[i].SpawnChansePerTime.Evaluate(_roadCounter.CurrentCount));
        }

        float value = UnityEngine.Random.Range(0, chances.Sum());
        float sum = 0;

        if (value > 0 && value < 0.05f)
            return _chunkPrefabs[UnityEngine.Random.Range(0, _chunkPrefabs.Count)];

        for (int i = 0; i < chances.Count; i++)
        {
            sum += chances[i];
            if (value < sum)
            {
                return _chunkPrefabs[i];
            }
        }

        return _chunkPrefabs[UnityEngine.Random.Range(0, _chunkPrefabs.Count)];
    }

    private void SubChunk(Chunk chunk)
    {
        chunk.OnChunkEnd += RemoveChunkWhenPass;
        chunk.OnChunkBegin += InstantiateChunk;
        chunk.FallController.OnPlayerFall += _character.KillPlayer;
    }

    private void UnsubChunk(Chunk chunk)
    {
        chunk.OnChunkEnd -= RemoveChunkWhenPass;
        chunk.OnChunkBegin -= InstantiateChunk;
        chunk.FallController.OnPlayerFall -= _character.KillPlayer;
    }

    private void RemoveChunkWhenPass(Chunk chunk)
    {
        UnsubChunk(chunk);
        _roadCounter.CurrentCount += 1;
        _spawnedChunks.Remove(chunk);
    }

    private void ResetSpawnedChunks()
    {
        foreach (Chunk chunk in _spawnedChunks) 
            UnsubChunk(chunk);

        _spawnedChunks.Clear();
    }

    public void RestartFactory()
    {
        foreach (Chunk chunk in _spawnedChunks)
            chunk.StartCoroutine(chunk.DestroyChunk(0));

        ResetSpawnedChunks();

        _startChunk.Init();
        SubChunk(_startChunk);
        _spawnedChunks.Add(_startChunk);

        for (int i = 0; i < _startChunkCount; i++)
            InstantiateChunk();
    }
}
