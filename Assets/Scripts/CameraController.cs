using Base;
using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : Singleton<CameraController>
{
    [SerializeField] private CinemachineVirtualCamera _virtualCamera;

    public void FocusCameraOnObject(GameObject obj)
    {
        _virtualCamera.ForceCameraPosition(obj.transform.position, Quaternion.identity);

        _virtualCamera.Follow = obj.transform;
        _virtualCamera.LookAt = obj.transform;
    }

    public void UnFocusCamera()
    {
        _virtualCamera.Follow = null;
        _virtualCamera.LookAt = null;
    }
}
